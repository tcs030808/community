## Serverless SIG

Serverless SIG致力于探索Serverless的技术发展与开源软件方案的实现

## 工作目标
持续优化Serverless系统的执行延迟、吞吐量与资源开销等。目前主要聚焦于Serverless计算框架等系统软件解决方案。Serverless SIG 期望通过社区合作，打造标准的Serverless开源解决方案。

## 邮件列表

serverless@lists.openkylin.top

## SIG成员
### Owner
- 赵来平(laiping@tju.edu.cn)

### Maintainers

