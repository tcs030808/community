
## Community 兴趣小组（SIG）

Community SIG组主要负责社区活动运营、基础设施及自媒体平台运营、品牌宣发推广、筹备社区架构会议事项等。

## 工作目标

- 负责策划组织社区活动（包括高校人才培养）
- 负责官网、论坛等基础设施平台
- 负责自媒体平台运营和品牌宣发
- 负责筹备社区架构会议

## SIG成员
### Owner
liumin@kylinos.cn
### Maintainers
xuyin@kylinos.cn
liumeiyan@kylinos.cn

## 邮件列表
communtiy@lists.openkylin.top