## OpenDDE SIG 

DDE (Deepin Desktop Enviroment) 是国内自主研发的一套桌面环境，致力于打造好用、易用的桌面环境。

OpenDDESIG致力于维护OpenKylin的桌面环境DDE相关组件，专注于打造美观易用、极简操作的桌面环境。主要负责桌面、启动器、任务栏、控制中心、窗口管理器等组件维护。

## 工作目标
移植DDE运行在OpenKylin
![目标1](mubiao1.jpeg)
![目标2](mubiao2.jpeg)


## SIG成员
* SIG社区负责人: DSOE1024
* SIG开发负责人: act365
* 包维护贡献者: chenchongbiao
* 包维护贡献者: u-answer-group-zhang-san
* 包维护贡献者: steve68

## 加入我们
#### 钉钉 KylinDDE SIG
![钉钉群](dingding.jpeg)

## SIG维护包列表

#### 桌面前端应用
- 画板: https://gitee.com/openkylin/deepin-draw
- 音乐: https://gitee.com/openkylin/deepin-music
- 相册: https://gitee.com/openkylin/deepin-album
- 相机: https://gitee.com/openkylin/deepin-camera
- 终端: https://gitee.com/openkylin/deepin-terminal
- 影院: https://gitee.com/openkylin/deepin-movie-reborn
- 编辑器: https://gitee.com/openkylin/deepin-editor
- 计算器: https://gitee.com/openkylin/deepin-calculator
- 下载器: https://gitee.com/openkylin/deepin-downloader
- 安装器: https://gitee.com/openkylin/deepin-deb-installer
- 控制中心: https://gitee.com/openkylin/dde-control-center
- 字体管理: https://gitee.com/openkylin/deepin-font-manager
- 图片预览: https://gitee.com/openkylin/deepin-image-viewer
- 截图录屏: https://gitee.com/openkylin/deepin-screen-recorder
- 打印管理器: https://gitee.com/openkylin/dde-printer
- 文件管理器: https://gitee.com/openkylin/dde-file-manager
- 日志查看器: https://gitee.com/openkylin/deepin-log-viewer
- 磁盘管理器: https://gitee.com/openkylin/deepin-diskmanager
- 设备管理器: https://gitee.com/openkylin/deepin-devicemanager
- 系统监视器: https://gitee.com/openkylin/deepin-system-monitor
- 备份还原工具: https://gitee.com/openkylin/deepin-clone
- 初次使用教程: https://gitee.com/openkylin/dde-introduction
- 显卡驱动管理器: https://gitee.com/openkylin/deepin-graphics-driver-manager
- 归档管理器(压缩管理): https://gitee.com/openkylin/deepin-compressor
- 启动盘制作工具(不推荐): https://gitee.com/openkylin/deepin-boot-maker
- 游戏
- 五子棋: https://gitee.com/openkylin/deepin-gomoku
- 连连看: https://gitee.com/openkylin/deepin-lianliankan

#### 桌面前端应用-附属组件
- 截图所需OCR: https://gitee.com/openkylin/deepin-ocr
- 剪贴板(右侧): https://gitee.com/openkylin/dde-clipboard
- 截图保存: https://gitee.com/openkylin/deepin-screensaver

#### 主题相关
- https://gitee.com/openkylin/deepin-icon-theme
- https://gitee.com/openkylin/deepin-cursor-theme
- https://gitee.com/openkylin/deepin-wallpapers
- https://gitee.com/openkylin/deepin-sound-theme
- https://gitee.com/openkylin/dde-wallpapers
- https://gitee.com/openkylin/deepin-gtk-theme

#### Deepin特色
- 登录所需的人脸识别: https://gitee.com/openkylin/dde-account-faces
- 人脸识别组件: https://gitee.com/openkylin/deepin-face
- 根目录AB卷: https://gitee.com/openkylin/deepin-ab-recovery

#### DDE窗体(DTK)组件
- https://gitee.com/openkylin/qt5platform-plugins
- https://gitee.com/openkylin/dtkcommon
- https://gitee.com/openkylin/dtkcore
- https://gitee.com/openkylin/dtkwidget
- https://gitee.com/openkylin/dtkgui
- https://gitee.com/openkylin/dtkdeclarative
- https://gitee.com/openkylin/qt5integration

#### DDE核心组件
- https://gitee.com/openkylin/dde-daemon
- https://gitee.com/openkylin/startdde
- https://gitee.com/openkylin/dde-dock
- https://gitee.com/openkylin/dde-kwin
- https://gitee.com/openkylin/deepin-kwin
- https://gitee.com/openkylin/dde-api
- https://gitee.com/openkylin/dde-launcher
- https://gitee.com/openkylin/dde-qt-dbus-factory
- https://gitee.com/openkylin/dde-daemon
- https://gitee.com/openkylin/dde-session-shell
- https://gitee.com/openkylin/dde-session-ui

#### DDE-X11组件
- https://gitee.com/openkylin/go-dbus-factory
- https://gitee.com/openkylin/go-x11-client

#### DDE-Wayland组件
- https://gitee.com/openkylin/dde-wayland-config

#### 其他(这里面的部分缺失描述)
- 应用商店<???>: https://gitee.com/openkylin/lastore-daemon
- 格式化工具(CLI)<什么鬼?>: https://gitee.com/openkylin/dde-device-formatter

- https://gitee.com/openkylin/deepin-manual
- https://gitee.com/openkylin/deepin-anything
- https://gitee.com/openkylin/gio-qt
- https://gitee.com/openkylin/udisks2-qt5
- https://gitee.com/openkylin/dde-grand-search
- https://gitee.com/openkylin/dareader
- https://gitee.com/openkylin/dde-wldpms
- https://gitee.com/openkylin/dde-wloutput-daemon
- https://gitee.com/openkylin/dde-wloutput
- https://gitee.com/openkylin/deepin-desktop-schemas
- https://gitee.com/openkylin/deepin-network-proxy
- https://gitee.com/openkylin/default-settings
- https://gitee.com/openkylin/dpa-ext-gnomekeyring
- https://gitee.com/openkylin/go-gir-generator
- https://gitee.com/openkylin/go-lib
- https://gitee.com/openkylin/dde-manual-content
- https://gitee.com/openkylin/dde-network-core
- https://gitee.com/openkylin/dde-polkit-agent
- https://gitee.com/openkylin/deepin-gettext-tools
- https://gitee.com/openkylin/deepin-pw-check
- https://gitee.com/openkylin/image-editor
- https://gitee.com/openkylin/deepin-picker
- https://gitee.com/openkylin/deepin-desktop-base
- https://gitee.com/openkylin/dde-app-services
- https://gitee.com/openkylin/deepin-voice-note
- https://gitee.com/openkylin/deepin-turbo
- https://gitee.com/openkylin/deepin-fcitxconfigtool-plugin
- https://gitee.com/openkylin/disomaster
- https://gitee.com/openkylin/docparser
- https://gitee.com/openkylin/deepin-reader