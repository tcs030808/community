# 安全技术 SIG

主要讨论在openKylin社区版本中已有或未来规划的安全技术：

- 在openKylin社区版本中使能主流的Linux安全特性，提供系统安全工具、库、基础设施等，提升系统的安全性
- 改善现有安全技术的应用体验，帮助安全创造实际的价值
- 讨论openKylin未来安全技术的规划


## 邮件列表

security@lists.openkylin.top

## SIG成员
### Owner
- 杨诏钧

### Maintainers
- Xie Fang(xiefang@kylinos.cn)
- 王玉成
