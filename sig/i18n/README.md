# SIG组名称
I18n(internationalization)

## SIG 工作目标
- 负责openkylin社区国际化和本地化相关工作，包括多语言开发框架，多语言平台开发和维护，社区和版本内词条、文档的翻译等。

## SIG 成员
### Owner
- handsome_feng

### Maintainer
- zouchang
- handsome_feng

## 邮件列表
i18n@lists.openkylin.top
