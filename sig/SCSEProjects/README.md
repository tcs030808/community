# SCSEProjects 小组 (SIG)

由上海交通大学网络空间安全学院负责项目和代码的维护，致力于将学院在隐私保护、AI安全等方面的研究成果移植到OpenKylin系统。

## 工作目标

- 负责隐私保护、AI安全、商密计算等方向的研究
- 负责将研究成果进行OpenKylin系统的移植和适配

## 邮件列表



## SIG成员

### Owner

- softds

### Maintainers


### Committers

## 仓库列表
- scse-projects

