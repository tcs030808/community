# 兼容能力 SIG

本SIG组致力于负责openKylin社区整机、外设、应用软件 兼容性相关能力探索。

## SIG 职责和目标

- 与硬件（整机、外设）兼容性，成立相关兼容性测试项目，进行操作系统与硬件兼容性的相关方面探索活动。
- 操作系统与应用程序兼容性策略探索、关键影响因素，类似白名单机制制定。
- 识别POSIX/lsb等组织对于兼容性的定义及规范性满足度审查。

## SIG 成员
### Owner
- James_2016 

## 邮件列表
compatibility@lists.openkylin.top

### Maintainers
- James_2016(mafajun@kylinos.cn)
- Feng Peipei(fengpeipei@kylinos.cn) 
- yexuefanghua（fanjiao@kylinos.cn）
- Judyzhu741011(zhuxiaohong@kylinos.cn)
